﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncObjects
{
    class BlockingCollectionExample
    {
        static BlockingCollection<Action> actions = new BlockingCollection<Action>();

        public static void ShowTest()
        {
            var stop = false;
            Barrier barrier = new Barrier(2);
            var thread = new Thread(() => {
                while (!stop)
                {
                    actions.Take()();
                }
                barrier.SignalAndWait();            
            });

            thread.Start();

            actions.Add(()=> stop=true);

            barrier.SignalAndWait();

            Console.WriteLine("Completed....");
        }

    }
}
