﻿namespace SyncObjects
{
    public static class MutexStartOnlyOneInstanceOfProgramExample
    {
        public static Mutex mutex = new System.Threading.Mutex(false, "MyUniqueMutexName");

        public static void Check()
        {
            
            try
            {
                if (mutex.WaitOne(0, true))
                {
                    // Run the application
                    Console.WriteLine("Application Start");
                }
                else
                {
                    Console.WriteLine("An instance of the application is already running.");
                }
            }
            catch(Exception e) 
            {
                Console.WriteLine(e.ToString());            
            }
            StopToShowExample();
        }

        private static void StopToShowExample()
        {
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();
        }


        public static void CloseMutex()
        {
            if (mutex != null)
            {
                mutex.Close();
                mutex = null;
            }
        }



    }
}
