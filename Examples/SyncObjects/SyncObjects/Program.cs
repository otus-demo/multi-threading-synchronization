﻿
using SyncObjects;


MutexStartOnlyOneInstanceOfProgramExample.Check();

//Console.WriteLine("Monitor example");
//var mo = new MonitorExample();
//mo.Show();
//mo.CountdownForWaitEndShowExample.Wait();

//Console.WriteLine("Mutex example");
//var me = new MutexExample();
//me.Show();
//me.CountdownForWaitEndShowExample.Wait();

//SyncBlockingIndexExample.SyncBlockIndexExample();

//Console.WriteLine("Semaphore example");
//var sem = new SemaphoreExample();
//sem.Show();
//sem.CountdownForWaitEndShowExample.Wait();

//Console.WriteLine("ReaderWriterLOck example");
//ReaderWriterLockExample.Show();

//Console.WriteLine("Difference between Lock and SpinLock example");
//var lsl = new LockExample();
//lsl.ShowDifferenceBetweenLockAndSpinLOck();

//Console.WriteLine("Difference between Lock and SpinLock example");
//SpinLockExample.Show();

//Console.WriteLine("Difference between Lock and SpinLock example");
//for (int i = 0; i < 10; i++)
//{
//    Console.WriteLine($"attempt #{i + 1} ----------");
//    SpinLockExamplePlus.Show();
//}



//Console.WriteLine("ManualResetEventSlim example");
//ManualResetEventSlimExample.Show();

//BlockingCollectionExample.ShowTest();

MutexStartOnlyOneInstanceOfProgramExample.CloseMutex();

Console.WriteLine("Press Enter to exit");
Console.ReadLine();
