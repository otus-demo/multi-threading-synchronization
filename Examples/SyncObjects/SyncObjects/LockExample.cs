﻿using System.Collections;
using System.Diagnostics;
using System.Threading;

namespace SyncObjects
{
    class LockExample
    {
        const int threadsAmount = 20;
        public static int numIteration = 100;

        public static TimeSpan timeout = TimeSpan.FromSeconds(500);

        public static Barrier barrier = new Barrier(threadsAmount);

        public static Barrier barrier2 = new Barrier(threadsAmount);

        public static Queue<int> SharedResource = new Queue<int>();


        public void ShowDifferenceBetweenLockAndSpinLOck()
        {

            CountdownEvent countdownEventLock = new CountdownEvent(threadsAmount);

            CountdownEvent countdownEventSlimLock = new CountdownEvent(threadsAmount);

            Object object1 = new Object();

            Stopwatch csLock = Stopwatch.StartNew();
            for (int i = 0; i < threadsAmount; i++)
            {
                var sm = new SummarizerOnLock(i, countdownEventLock);
            }
            countdownEventLock.Wait();
            csLock.Stop();

            Console.WriteLine($"Lock: {csLock.ElapsedMilliseconds} milliseconds");

            

            Stopwatch csMonitor = Stopwatch.StartNew();
            for (int i = 0; i < threadsAmount; i++)
            {
                var sm = new SummarizerOnSpinLock(i, countdownEventSlimLock);
            }
            countdownEventSlimLock.Wait();
            csMonitor.Stop();

            Console.WriteLine($"Monitor.TryEnter: {csMonitor.ElapsedMilliseconds} milliseconds"); ;
        }
    }



    class SummarizerOnLock
    {
        public static int commonResource = 0;

        public static object obj = new object();

        CountdownEvent countdown;

        public SummarizerOnLock(int i, CountdownEvent cntdown)
        {
            countdown = cntdown;
            Thread mythread = new Thread(Run);
            mythread.Name = $"Поток на Lock {i}";            
            mythread.Start();

        }

        public void Run()
        {
            LockExample.barrier.SignalAndWait();
            for (int i = 0; i < LockExample.numIteration; i++)
            {
                //lock (obj)
                //{
                //    LongOperation();
                //}
                Monitor.Enter(obj);
                LongOperation();
                Monitor.Exit(obj);

            }
            countdown.Signal();
        }

        void LongOperation()
        {
            commonResource = 0;
            while (commonResource < 1_000_000)
                commonResource++;
            LockExample.SharedResource.Enqueue(commonResource);

            //Thread.Sleep(1);
        }
    }


    class SummarizerOnSpinLock
    {
        public static int commonResource = 0;

        public static object obj = new object();

        static SpinLock sl = new SpinLock();

        static TimeSpan timeout = TimeSpan.FromMilliseconds(0);

        bool lockTaken;

        CountdownEvent countdown;

        public SummarizerOnSpinLock(int i, CountdownEvent cntdown)
        {
            countdown = cntdown;
            Thread mythread = new Thread(Run);
            mythread.Name = $"Поток на SpinLock {i}";
            mythread.Start();

        }

        public void Run()
        {
            LockExample.barrier2.SignalAndWait();
            for (int i = 0; i < LockExample.numIteration; i++)
            {
                
                bool lockTaken = false;

                try
                {
                    Monitor.TryEnter(obj, LockExample.timeout, ref lockTaken);
                    if (lockTaken)
                        LongOperation();
                    else
                        Console.WriteLine("Timeout had happen");
                }
                finally 
                {
                    if (lockTaken)
                        Monitor.Exit(obj);
                }
                


            }
            countdown.Signal();
        }

        void LongOperation()
        {
            commonResource = 0;
            while (commonResource<1_000_000)
            commonResource++;
            LockExample.SharedResource.Enqueue(commonResource);
            //Thread.Sleep(0);
        }
    }
}
