﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncObjects
{
    internal class SemaphoreExample
    {
        public CountdownEvent CountdownForWaitEndShowExample { get; set; } = new CountdownEvent(5);

        public void Show()
        {
            // запускаем пять потоков
            for (int i = 1; i < 6; i++)
            {
                Reader reader = new Reader(i, CountdownForWaitEndShowExample);
            }
        }
    }

    /// <summary>
    /// В библиотеке есть место только для трех читателей
    /// </summary>
    class Reader
    {
        // создаем семафор
        static Semaphore sem = new Semaphore(3, 3);
        Thread myThread;
        int count = 3;// количество обязательных посещений библиотеки
        CountdownEvent countdownForServiceEvent;

        public Reader(int i, CountdownEvent countdownEvent)
        {
            countdownForServiceEvent = countdownEvent;
            myThread = new Thread(Read);
            myThread.Name = $"Читатель {i}";
            myThread.Start();
        }

        public void Read()
        {
            while (count > 0)
            {
                sem.WaitOne();  // ожидаем, когда освободиться место

                Console.WriteLine($"{Thread.CurrentThread.Name} входит в библиотеку");

                Console.WriteLine($"{Thread.CurrentThread.Name} читает");
                Thread.Sleep(1000);

                Console.WriteLine($"{Thread.CurrentThread.Name} покидает библиотеку");

                sem.Release();  // освобождаем место

                count--;
                Thread.Sleep(1000);
            }

            countdownForServiceEvent.Signal();
        }
    }

}
